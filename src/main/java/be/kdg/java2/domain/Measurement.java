package be.kdg.java2.domain;

import be.kdg.java2.util.JAXBLocalDateTimeAdapter;
import com.google.gson.annotations.SerializedName;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import java.time.LocalDateTime;
import java.util.Random;

public class Measurement {
    //gebruik transient als je een veld niet naar json wil sturen
    private int temperature;//between 0-30
    private int humidity;//percentage
    @SerializedName("co")//hernoemen voor GSON library...
    private int coConcentration;//between 20-2000 (ppm)
    private LocalDateTime timestamp;

    public Measurement(int temperature, int humidity, int coConcentration, LocalDateTime timestamp) {
        this.temperature = temperature;
        this.humidity = humidity;
        this.coConcentration = coConcentration;
        this.timestamp = timestamp;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public int getCoConcentration() {
        return coConcentration;
    }

    public void setCoConcentration(int coConcentration) {
        this.coConcentration = coConcentration;
    }

    @XmlAttribute
    @XmlJavaTypeAdapter(JAXBLocalDateTimeAdapter.class)
    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public static Measurement randomMeasurement() {
        Random r = new Random();
        int temp = r.nextInt(0, 30);
        int humidity = r.nextInt(20, 60);
        int coConcentration = r.nextInt(20, 2000);
        LocalDateTime timestamp = LocalDateTime.now();
        return new Measurement(temp, humidity, coConcentration, timestamp);
    }
}
