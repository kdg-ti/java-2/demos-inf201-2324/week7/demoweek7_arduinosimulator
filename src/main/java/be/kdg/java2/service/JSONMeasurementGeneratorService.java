package be.kdg.java2.service;

import be.kdg.java2.domain.Measurement;
import be.kdg.java2.util.GSonLocalDateTimeAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileNotFoundException;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.Formatter;
import java.util.List;
import java.util.stream.Stream;

public class JSONMeasurementGeneratorService implements MeasurementGeneratorService{
    @Override
    public void generate(int deviceId, int count) {
        List<Measurement> measurementList = Stream.generate(Measurement::randomMeasurement)
                .limit(count)
                .sorted(Comparator.comparing(Measurement::getTimestamp))
                .toList();
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setPrettyPrinting();
        gsonBuilder.registerTypeAdapter(LocalDateTime.class, new GSonLocalDateTimeAdapter().nullSafe());
        Gson gson = gsonBuilder.create();
        String jsonString = gson.toJson(measurementList);
        try {
            Formatter formatter = new Formatter("device" + deviceId + ".json");
            formatter.format(jsonString);
            formatter.close();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
