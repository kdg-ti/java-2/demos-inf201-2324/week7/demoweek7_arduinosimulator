package be.kdg.java2.service;

import be.kdg.java2.domain.Measurement;
import be.kdg.java2.domain.Measurements;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;

import java.io.File;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

public class XMLMeasurementGeneratorService implements MeasurementGeneratorService {
    @Override
    public void generate(int deviceId, int count){
        List<Measurement> measurementList = Stream.generate(Measurement::randomMeasurement)
                .limit(count)
                .sorted(Comparator.comparing(Measurement::getTimestamp))
                .toList();
        try {
            JAXBContext jc = JAXBContext.newInstance(Measurements.class);
            Marshaller marshaller = jc.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            Measurements measurements = new Measurements();
            measurements.setMeasurements(measurementList);
            marshaller.marshal(measurements, new File("device" + deviceId + ".xml"));
        } catch (JAXBException e) {
            throw new RuntimeException(e);
        }
    }
}
