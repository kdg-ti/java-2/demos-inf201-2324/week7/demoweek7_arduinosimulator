package be.kdg.java2.service;

public interface MeasurementGeneratorService {
    void generate(int deviceId, int count);
}
