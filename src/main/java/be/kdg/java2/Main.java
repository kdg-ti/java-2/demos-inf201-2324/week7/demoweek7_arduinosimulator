package be.kdg.java2;

import be.kdg.java2.service.JSONMeasurementGeneratorService;
import be.kdg.java2.service.MeasurementGeneratorService;
import be.kdg.java2.service.XMLMeasurementGeneratorService;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello gradle!");
        //MeasurementGeneratorService generatorService = new XMLMeasurementGeneratorService();
        MeasurementGeneratorService generatorService = new JSONMeasurementGeneratorService();
        generatorService.generate(1, 24);
        generatorService.generate(2, 24);
        generatorService.generate(3, 24);
    }
}
